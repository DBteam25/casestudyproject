import json, pprint, sseclient, requests
from flask import Flask, Response, Request
app = Flask(__name__)

@app.route('/')
def pass_stream():
    url = 'http://127.0.0.1:8080/rawdeals'
    response = requests.get(url, stream=True)  # or with_requests(url)
    client = sseclient.SSEClient(response)
    print("")
    def eventStream():
        print('new eventstream')
        for event in client.events():
            print(event)
            pprint.pprint(json.loads(event.data))
            yield event.data

    return Response(eventStream(), mimetype="text/event-stream")

if __name__ == '__main__':
    app.run(port=8081, threaded=True, debug=True)
