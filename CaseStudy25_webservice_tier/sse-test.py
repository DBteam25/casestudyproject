import json
import pprint
import sseclient
import requests
def with_requests(url):
    """Get a streaming response for the given event feed using requests."""
    return requests.get(url, stream=True)

url = 'http://127.0.0.1:8080/rawdeals'
response = with_requests(url)  # or with_requests(url)
client = sseclient.SSEClient(response)
for event in client.events():
    pprint.pprint(json.loads(event.data))
    