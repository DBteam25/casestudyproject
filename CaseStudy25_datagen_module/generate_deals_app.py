import RandomDealData
from flask import Flask, Response

app = Flask(__name__)
random_deals_instance = RandomDealData.RandomDealData()


@app.route("/rawdeals")
def get_random_deals():
    instruments = random_deals_instance.createInstrumentList()

    def deal_stream():
        while True:
            data = get_random_data(instruments)
            yield "data:" + data + "\n\n"

    return Response(deal_stream(), status=200, mimetype="text/event-stream")


def get_random_data(instruments):
    random_deals = random_deals_instance.createRandomData(instruments)
    return random_deals


def bootapp():
    # when dockerizing this, uncomment the following line
    # app.run(port=8080, threaded=True, host=('0.0.0.0'))

    # when dockerizing this, comment the following line
    app.run(port=8080, threaded=True, host='0.0.0.0')
