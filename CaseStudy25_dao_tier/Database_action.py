import mysql.connector as mysql

class Dao:
    def __init__(self):
        self.connection = mysql(host='',database='db_25',user='root',password='ppp')
        self.cursor = self.connection.cursor(prepared=True)

    def insert_user_account(self,name,surname,e_mail,role_id):
        sql_query = "INSERT INTO user_accounts (name,surname,e_mail,role_id) VALUES (%s,%s,%s,%s);"
        insert_tuple=(name,surname,e_mail,role_id)
        result = self.cursor.execute(sql_query, insert_tuple)
        self.connection.commit()

    def insert_roles(self, role_name):
        sql_query = "INSERT INTO roles (role_name) VALUES (%s);"
        result = self.cursor.execute(sql_query, role_name)
        self.connection.commit()

    def insert_counter_parties(self, name_counter_party):
        sql_query = "INSERT INTO counter_parties (name_counter_party) VALUES (%s);"
        result = self.cursor.execute(sql_query,name_counter_party)
        self.connection.commit()

    def insert_instruments(self, name_instrument):
        sql_query = "INSERT INTO instruments (name_instrument) VALUES (%s);"
        result = self.cursor.execute(sql_query, name_instrument)
        self.connection.commit()

    def insert_types(self, name_type):
        sql_query = "INSERT INTO types (name_type) VALUES (%s);"
        result = self.cursor.execute(sql_query,name_type)
        self.connection.commit()

    def insert_deals(self,instrument_id, counter_party_id, price, type_id, quantity, time):
        sql_query = "INSERT INTO deals (instrument_id,counter_party_id,price,type_id,quantity) VALUES (%s,%s,%s,%s,%s);"
        result = self.cursor.execute(sql_query,instrument_id,counter_party_id,price,type_id,quantity,time)
        self.connection.commit()

    def delete_elem_from_user_account(self, user_account_id):
        sql_query = "DELETE FROM user_account WHERE user_account_id = (%s)"
        result = self.cursor.execute(sql_query,user_account_id)
        self.connection.commit()

    def delete_elem_from_roles(self,role_id):
        sql_query = "DELETE FROM roles WHERE role_id = (%s)"
        result = self.cursor.execute(sql_query,role_id)
        self.connection.commit()
    def delete_elem_from_counter_parties(self,counter_party_id):
        sql_query = "DELETE FROM counter_parties WHERE counter_party = (%s)"
        self.cursor.execute(sql_query,counter_party_id)
        self.connection.commit()
    def delete_elem_from_instruments(self,instrument_id):
        sql_query = "DELETE FROM instruments WHERE instrument_id = (%s)"
        self.cursor.execute(sql_query,instrument_id)
        self.connection.commit()
    def delete_elem_from_types(self, type_id):
        sql_query = "DELETE FROM types WHERE type_id = (%s)"
        self.cursor.execute(sql_query,type_id)
        self.connection.commit()
    def delete_elem_from_deals(self, deal_id):
        sql_squery = "DELETE FROM deals WHERE deal_id = (%s)"
        self.cursor.execute(sql_squery,deal_id)
        self.connection.commit()
    def __del__(self):
        self.cursor.close()
        self.connection.close()
