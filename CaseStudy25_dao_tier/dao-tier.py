import mysql.connector as mysql
import json
from datetime import datetime

HOST = "192.168.99.100"

def get_instrument_id_by_name(instrument_name, cursor):
    query_for_instrument_id = "select instrument_id from instruments where instrument_name = %s"
    cursor.execute(query_for_instrument_id, (instrument_name,))
    try:
        ((result,),) = cursor.fetchall()
    except:
        raise ValueError()

    return result

def get_counter_party_id_by_name(counter_party_name, cursor):
    while True:
        query_for_counter_party_id = "select counter_party_id from counter_parties where counter_party_name = %s"
        cursor.execute(query_for_counter_party_id, (counter_party_name,))

        try:
            ((result,),) = cursor.fetchall()
            break
        except:
            query_for_new_counter_party = "insert into counter_parties (counter_party_name) values (%s)"
            cursor.execute(query_for_new_counter_party, (counter_party_name,))

    return result

def get_type_id_by_name(type_name, cursor):
    query_for_type_id = "select type_id from types where type_name = %s"
    cursor.execute(query_for_type_id, (type_name,))
    try:
        ((result,),) = cursor.fetchall()
    except:
        raise ValueError()

    return result

def insert_deals_to_mysql_database_from_one_json_object(json_object):
    db = mysql.connect(
        host=HOST,
        user="root",
        passwd="ppp",
        database="db_25"

    )
    cursor = db.cursor()

    instrument_name = json_object['instrumentName']
    counter_party_name = json_object['cpty']
    price = json_object['price']
    type_name = json_object['type']
    quantity = json_object['quantity']
    time = json_object['time']

    instrument_id = get_instrument_id_by_name(instrument_name, cursor)
    counter_party_id = get_counter_party_id_by_name(counter_party_name, cursor)
    type_id = get_type_id_by_name(type_name, cursor)
    datetime_in_a_good_format = datetime.strptime(time, '%d-%b-%Y (%H:%M:%S.%f)')

    query = "insert into deals (instrument_id, counter_party_id, price, type_id, quantity, time) " \
            "values(%s, %s, %s, %s, %s, %s)"
    values = (instrument_id, counter_party_id, price, type_id, quantity, datetime_in_a_good_format)

    try:
        cursor.execute(query, values)
        db.commit()
    except:
        db.rollback()
        print("errorrrr")

    cursor.close()


def insert_deals_to_mysql_database_from_json(json_file):
    with open(json_file) as json_file:
        data = json.load(json_file)
        print(data)
        for one_object in data:
            insert_deals_to_mysql_database_from_one_json_object(one_object)

#insert_deals_to_mysql_database_from_json('dirty_data.json')

def select_deals_from_time_interval(start, end):
    db = mysql.connect(
        host=HOST,
        user="root",
        passwd="ppp",
        database="db_25"
    )
    cursor = db.cursor()

    query = "SELECT r.deal_id, l.instrument_name, c.counter_party_name, r.price, t.type_name, "  \
            "r.quantity, r.time FROM instruments l LEFT JOIN deals r using(instrument_id) LEFT JOIN " \
            "types t using(type_id) LEFT JOIN counter_parties c using(counter_party_id) " \
            "where r.time >= %s and r.time <= %s;"
    values = (start, end)
    try:
        cursor.execute(query, values)
    except:
        print("errorrrr")

    res = cursor.fetchall()
    cursor.close()
    return res


def select_average_price_for_each_instrument(type_name):
    if type_name not in ('B', 'S'):
        raise ValueError()
    else:
        db = mysql.connect(
            host=HOST,
            user="root",
            passwd="ppp",
            database="db_25"
        )
        cursor = db.cursor()

        query = "SELECT l.instrument_name, AVG(r.price) " \
                "FROM instruments l " \
                "LEFT JOIN deals r using(instrument_id) " \
                "LEFT JOIN types t using(type_id) " \
                "WHERE t.type_name = %s " \
                "GROUP BY l.instrument_name;"
        values = (type_name, )
        try:
            cursor.execute(query, values)
        except:
            print("errorrrr")

        res = cursor.fetchall()
        cursor.close()
        return res


def select_all_data():
        db = mysql.connect(
            host=HOST,
            user="root",
            passwd="ppp",
            database="db_25"
        )
        cursor = db.cursor()

        query = "SELECT r.deal_id, l.instrument_name, c.counter_party_name, r.price, t.type_name, "  \
                "r.quantity, r.time FROM instruments l LEFT JOIN deals r using(instrument_id) LEFT JOIN " \
                "types t using(type_id) LEFT JOIN counter_parties c using(counter_party_id);"
        values = ()
        try:
            cursor.execute(query, values)
        except:
            print("errorrrr")

        res = cursor.fetchall()
        cursor.close()
        return res

#insert_deals_to_mysql_database_from_json('another_dirty_data.json')
#print(select_deals_from_time_interval('2019-08-11 12:07:06', '2019-08-11 12:07:07'))
#print(select_average_price_for_each_instrument('B'))
print(select_all_data())

